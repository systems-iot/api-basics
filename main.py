from fastapi import FastAPI

app = FastAPI()


robots = {
    "A123": {
        "name": "rambo",
        "color": "blue"
    },
    "B456": {
        "name": "dave",
        "color": "red"
    }
}

@app.get("/hello")
def say_hello():
    return {"hello": "world"} 


@app.get("/robots")
def get_robots():
    return robots

@app.get("/robots/{id}")
def get_robot_by_id(id):
    return robots[id]


@app.post("/robots")
def get_robot_by_id(body: dict):

    id = body["id"]
    robots[id] = body

    return body