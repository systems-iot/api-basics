import requests as r

res = r.get("http://localhost:8080/robots")
j = res.json()
print(j)


robot = {
    "id": "D678",
    "name": "sally2",
    "color": "purple or pink"
}
res = r.post("http://localhost:8080/robots", json=robot)


res = r.get(f'http://localhost:8080/robots/{robot["id"]}')
j = res.json()
print(j)