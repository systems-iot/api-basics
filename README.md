# API Basics

[Video here](https://youtu.be/K114qoMt_UE)

To clone this repo, open a terminal in a folder of your choice and enter:

```
git clone https://gitlab.com/systems-iot/api-basics
```

Then `cd api-basics` to enter the folder.  Once in the folder, run:

```
python3 -m venv venv
```
to create a virtual environment.  Then install the dependencies:

```
pip install -r requirements.txt

```

Once you do that, simply run the server:
```
uvicorn main:app --reload --port 8080
```

and the client:

```
python client.py
```
